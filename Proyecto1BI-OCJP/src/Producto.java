import java.util.ArrayList;
import java.util.TreeMap;

//Clase Producto (Hamburguesa, Gaseosa, etc)
public abstract class Producto {
	private String strNombre, strTipo;
	private double dblPrecio; 
	
	public Producto (String strNombre, String strTipo, double dblPrecio){
		this.strNombre = strNombre;
		this.strTipo = strTipo;
		this.dblPrecio = dblPrecio;
	}
	
	public Producto (){
		this.strNombre = "";
		this.strTipo = "";
		this.dblPrecio = 0.0;
	}

	public String getStrNombre() {
		return strNombre;
	}

	public void setStrNombre(String strNombre) {
		this.strNombre = strNombre;
	}

	public String getStrTipo() {
		return strTipo;
	}

	public void setStrTipo(String strTipo) {
		this.strTipo = strTipo;
	}

	public double getDblPrecio() {
		return dblPrecio;
	}

	public void setDblPrecio(double dblPrecio) {
		this.dblPrecio = dblPrecio;
	}
	
	static class CajaFeliz {
		
		static TreeMap <String, Integer> nombreFigura = new TreeMap<String, Integer>();
		
		private void anadirFigura(String strNombreFigura, int intCantidad){
			int i = nombreFigura.get(strNombreFigura);
			nombreFigura.put(strNombreFigura, intCantidad+i);
		}
		
		private void quitarFigura(String strNombreFigura, int intCantidad){
			int i = nombreFigura.get(strNombreFigura);
			nombreFigura.put(strNombreFigura, i-1);
		}
		
	}
	
}