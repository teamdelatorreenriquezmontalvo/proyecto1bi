 
//Aderezos que se le puede poner a los diferentes productos (Hamburgueza, Nuggets, etc)
public interface Aderezo {
	
	//SobreCarga
	public void ponerSalsaDeTomate(int n);
	public void ponerSalsaDeTomate();
	
	public void ponerMayonesa(int n);
	public void ponerMayonesa();
	
	public void ponerMostaza(int n);
	public void ponerMostaza();
	
}

enum TipoAderezo{
	SALSADETOMATE, MAYONESA, MOSTAZA;
}