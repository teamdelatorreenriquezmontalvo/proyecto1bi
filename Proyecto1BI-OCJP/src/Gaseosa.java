
public class Gaseosa extends Producto{
	String strMarca;
	double dblTamanio;
	

	public Gaseosa(String strMarca, double dblPrecio, double dblTamanio) {
		super(strMarca, "Gaseosa", dblPrecio);
		this.strMarca = strMarca;
		this.dblTamanio = dblTamanio;
	}
	
	public String getStrMarca() {
		return strMarca;
	}
	public void setStrMarca(String strMarca) {
		this.strMarca = strMarca;
	}
	public double getDblTamanio() {
		return dblTamanio;
	}
	public void setDblTamanio(double dblTamanio) {
		this.dblTamanio = dblTamanio;
	}
	
}

enum marcaGaseosa {
	Coca_Cola, Sprite, Fanta, Pepsi
}