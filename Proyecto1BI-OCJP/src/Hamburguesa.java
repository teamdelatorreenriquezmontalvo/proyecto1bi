

final public class Hamburguesa extends Producto implements Aderezo{
	
	private String strTipoPan;
	int cantidadDeHamburguesas = 1000;
	
	public Hamburguesa(String strNombre, double dblPrecio, String strTipoPan){
		super(strNombre, "Hamburguesa", dblPrecio);
		
		this.strTipoPan = strTipoPan;
	}

	//ETODOS DE LA INTERFACE Aderezo
	@Override
	public void ponerSalsaDeTomate(int n) {
		System.out.println("se puso " + n + " " + TipoAderezo.SALSADETOMATE);
	}
	@Override
	public void ponerSalsaDeTomate() {
		System.out.println("se puso 1 SALSADETOMATE");
	}

	@Override
	public void ponerMayonesa(int n) {
		System.out.println("se puso " + n + " " + TipoAderezo.MAYONESA);
	}
	@Override
	public void ponerMayonesa() {
		System.out.println("se puso 1 MAYONESA");
		
	}

	@Override
	public void ponerMostaza(int n) {
		System.out.println("se puso " + n + " " + TipoAderezo.MOSTAZA);
	}
	@Override
	public void ponerMostaza() {
		System.out.println("se puso 1 MOSTAZA");
		
	}
	
	public void hayHamburguesa() {
		class ExistenciaDeHamburguesa{
			public int cantidadDeHamburguesas(int numHamburguesasVendidas){
				return (cantidadDeHamburguesas - numHamburguesasVendidas);
			}
		}
	}
}

enum TipoHamburguesa{
	McNifica, BigMac, CuartoDeLibra
}