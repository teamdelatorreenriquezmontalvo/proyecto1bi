
//Al Combo PerteceHamburguesa, Gaseosa, Papas, etc
public class Combo {
	protected Hamburguesa hamburguesa;
	protected Gaseosa gaseosa;
	protected double dblPrecio;
	
	static long numeroDeCombosvendidos = 0;
	
	public Combo(){
		
	}
	
	public Combo(Hamburguesa hamburguesa){
		this.hamburguesa = hamburguesa;
		this.gaseosa = new Gaseosa("La Que Sea", 1.75, 0.5);
		this.dblPrecio = (hamburguesa.getDblPrecio() + gaseosa.getDblPrecio()) * 0.75;
		++numeroDeCombosvendidos;
	}
	
	public Hamburguesa getHamburguesa() {
		return hamburguesa;
	}

	public void setHamburguesa(Hamburguesa hambuguesa) {
		this.hamburguesa = hambuguesa;
	}

	public Gaseosa getGaseosa() {
		return gaseosa;
	}

	public void setGaseosa(Gaseosa gaseosa) {
		this.dblPrecio = (this.hamburguesa.getDblPrecio() + gaseosa.getDblPrecio())*0.8;
		this.gaseosa = gaseosa;
	}

	public double getDblPrecio() {
		return dblPrecio;
	}

	public void setDblPrecio(double dblPrecio) {
		this.dblPrecio = dblPrecio;
	}
	
	public static Combo elegirComboSuper(){
		return new Combo(new Hamburguesa(TipoHamburguesa.McNifica.name(), 4, "Suave"));
	}
	
	public static Combo elegirComboMega(){
		return new Combo(new Hamburguesa(TipoHamburguesa.BigMac.name(), 5, "Suave"));	
	}
	
	public static Combo elegirComboCuarto(){
		return new Combo(new Hamburguesa(TipoHamburguesa.CuartoDeLibra.name(), 4.5, "Suave"));
	}
	
	/*public static void main(String[] args) {
		Hamburguesa h1 = new Hamburguesa("McNifica", 4, "Pan-Suave");
		Combo combo1 = Combo.elegirComboSuper();
		
		combo1.setGaseosa(new Gaseosa(marcaGaseosa.Pepsi.name(), 2.2, 1));
		
		System.out.println(combo1.getDblPrecio());
		
	}*/
}
