import java.util.ArrayList;

public class Orden extends Thread{

	private ArrayList<Combo> listCombo;
	private ArrayList<Hamburguesa> listHamburguesa;
	private ArrayList<Gaseosa> listGaseosa;
	static int numOrden=0;
	
	public Orden(){
		this.listCombo = new ArrayList<Combo>();
		this.listHamburguesa= new ArrayList<Hamburguesa>();
		this.listGaseosa = new ArrayList<Gaseosa>();
	}
	public void agregarCombo(Combo c){
		
		this.listCombo.add(c);
	}
	public void agregarHamburguesa(Hamburguesa h){
		this.listHamburguesa.add(h);
	}
	public void agregarGaseosa(Gaseosa g){
		this.listGaseosa.add(g);
	}
	
	
	public ArrayList<Combo> getListCombo() {
		return listCombo;
	}
	public void setListCombo(ArrayList<Combo> listCombo) {
		this.listCombo = listCombo;
	}
	public ArrayList<Hamburguesa> getListHamburguesa() {
		return listHamburguesa;
	}
	public void setListHamburguesa(ArrayList<Hamburguesa> listHamburguesa) {
		this.listHamburguesa = listHamburguesa;
	}
	public ArrayList<Gaseosa> getListGaseosa() {
		return listGaseosa;
	}
	public void setListGaseosa(ArrayList<Gaseosa> listGaseosa) {
		this.listGaseosa = listGaseosa;
	}
	
	public static void testAssertNumOrden(int n){
		assert n < 101 : "m es: " + 101 + " 	...comenenzar desde 1";
	}
	
	public void run() {
		byte numGaseosas = 0;
		byte numHamMcNifica = 0;
		byte numHamBigMac = 0;
		byte numHamCuarto = 0;
		short tiempoEntrega = 0;
		short tiempoServirGaseosa = 60;
		short tiempoServirMacNifica = 400;
		short tiempoServirBigMac = 300;
		short tiempoServirCuarto = 600;
		
		
				
		for (int i = 0; i < listCombo.size();i++){
			if(TipoHamburguesa.McNifica.name() == listCombo.get(i).getHamburguesa().getStrNombre()) {
				numHamMcNifica++;
				numGaseosas++;
				}
			else if (TipoHamburguesa.BigMac.name() == listCombo.get(i).getHamburguesa().getStrNombre()) {
				numHamBigMac++;
				numGaseosas++;
				}
			else {
				numHamCuarto++;
				numGaseosas++;
				}
		}
	
		for (int i=0;i < listGaseosa.size();i++){
			numGaseosas++;
		} 
	
		for (int i=0;i < listHamburguesa.size();i++){
			if(TipoHamburguesa.McNifica.name() == listHamburguesa.get(i).getStrNombre()) {
				numHamMcNifica++;
				}
			else if (TipoHamburguesa.BigMac.name() == listHamburguesa.get(i).getStrNombre()) {
				numHamBigMac++;
				}
			else {
				numHamCuarto++;
				}
		} 
		
		try {
			tiempoEntrega =(short) ((numGaseosas*tiempoServirGaseosa)+ (numHamMcNifica*tiempoServirMacNifica)+ (numHamBigMac*tiempoServirBigMac)+ (numHamCuarto*tiempoServirCuarto));
            
			sleep(tiempoEntrega);
        }
        catch (InterruptedException e) {

        }
		
		System.out.println("\nTiempo de entrega :"+tiempoEntrega + "seg");

       
	}
	
}
